try:
  f = open('nginx_log.txt')
  print('log file is readable\n')

  data = [ [line.split(" ")[0],line.split(" ")[9]] for line in f.readlines() ]
  uniq_addresses = set([ address[0] for address in data ])

  dict_byte_sum = {}

  for address in uniq_addresses:
    list_byte = [ int(value[1]) for value in data if value[0] == address ]
    dict_byte_sum[sum(list_byte)] = address

  sorted_bytes_list = reversed(sorted(dict_byte_sum))
  dict_sorted = { x:dict_byte_sum[x] for x in sorted_bytes_list  }

  print("top 10 downloaders")
  index = 0
  for key in dict_sorted:
    if index == 10:
      break
    else:
      print("host = ",dict_sorted[key]," bytes = ",key)
    index = index+1

except Exception as E:
	print(E)


