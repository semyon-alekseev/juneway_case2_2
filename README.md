# juneway_case2_2
# TEST case 2_2 for the study project JuneWay
***
*This repo contains files needed to show the top 10 downloaders found in a nginx log*
***
# Content
- top10.py
- nginx_log.txt
***
# Tested on:
- [X] Win10

