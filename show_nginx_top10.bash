#!/bin/bash
echo "test git"
file="nginx_log.txt"

x=0
y=0

if [ -f $file ]; then 
  while read LINE;do
    read a[x] a[x+1]  <<< $(echo $LINE | awk '{print $1" "$10}')
    b[y]=${a[x]}
    let x=x+2
    let y=y+1
  done < $file
else
  echo "there is no log file "
  break
fi

echo -e "adrresses and bytes\n" ${a[@]}
echo
echo -e "list of host addresses\n" ${b[@]}

c[0]=${b[0]}
index=1
for (( i=1; i<=${#b[@]}; i++ ));do
  for (( y=0; y<=${#c[@]}; y++ ));do
    mark=0
    if [ "${c[y]}" == "${b[i]}" ];then
      mark="1"
      break
    fi
  done
  if [ "$mark" != "1" ];then
    c[index]=${b[i]}
    let index=index+1
  fi
done

for (( i=0; i<${#c[@]}; i++ ));do
  address_sum=0
  for (( y=0; y<${#a[@]}; y += 2 ));do
    if [ "${a[y]}" == "${c[i]}" ];then
      let address_sum=address_sum+a[y+1]
    fi
  done
  d[i*2]=${c[i]}
  d[i*2+1]=$address_sum
done

echo "unsorted loaders "
for (( i=0; i<${#d[@]}; i += 2 ));do
  echo ${d[i]}" "${d[i+1]}
done

echo "top 10 loaders "
for (( i=0; i<${#d[@]}; i += 2 ));do
  a=0
  b=0
  for (( y=0; y<${#d[@]}; y += 2 ));do
    if [ ${d[y+1]} -lt ${d[i+1]} ];then
      a=${d[y]}
      b=${d[y+1]}
      d[y]=${d[i]}
      d[y+1]=${d[i+1]}
      d[i]=$a
      d[i+1]=$b
    fi
  done
done

for (( i=0; i<20; i += 2 ));do
  echo ${d[i]}" "${d[i+1]}
done
